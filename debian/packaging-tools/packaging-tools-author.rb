#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
#
# Extract copyright holders.
# Author:   Daigo Moriwaki <beatles@sgtpepper.net>
# Copyright (C) 2005 Daigo Moriwaki
#           (C) 2015 Youhei SASAKI
# License:  GNU GENERAL PUBLIC LICENSE Version 2 or later.
#
require 'pp'
def update_hash(hash, key, val)
  @hash = hash
  @key = key
  @val = val
  unless @hash[@key]
    @hash[@key] = @val
  else
    unless @hash[@key] == ""
      new = @hash[@key] + ", " + @val
    else
      new = @val
    end
    @hash[@key] = new
  end
  return @hash
end

def cleanup_author_year(str)
  @str = str
  @str = @str.gsub(/,/," ")
  @str = @str.gsub(/all right reserved/i,"")
  @str = @str.gsub(/\sby\s/,'')
  @str = @str.gsub(/notices:/,'')
  @str = @str.gsub(/\s+/," ")
  # case @str
  # when /^(\d{4})-(\S.*)/
  #   @str = $1 + "-" + $2
  # # when /^(\d{4})\s\d{4}\s(\d{4}.*)/
  # #   @str = $1 + "-" + $2
  # when /^(\d{4})(\S.*)/
  #   @str = $1 + " " + $2.gsub(/^-\s/,'')
  # when /(.*)\s(\d{4})/
  #   @str = $2 + " " + $1
  # end
  # if @str =~/(\d{4})(\S.*)/
  #   left,right = $1.strip, $2.strip
  #   if right =~ /^-/
  #     @str = left + right
  #   elsif right=~/^(\d{4}.*)/
  #     @str = left + "-" + $1
  #   else
  #     @str = left.gsub(/-$/,'') + " " + right
  #   end
  # elsif
  # end
  return @str
end

def file_grep
  lines = IO.popen(
    "find . -type f \|
      xargs grep -i copyright \|
      grep -v debian \|
      grep -v .git \|
      grep -v LICENSE \|
      grep -v ChangeLog \|
      grep -v misc/plugin/title_tag.rb \|
      grep -v misc/plugin/makerss.rb
    " ) {
    |io| io.readlines
  }
  @copyright = Hash.new
  @unknown = Hash.new
  @authors = Array.new
  lines.each do |l|
    @fname = l.split(":")[0].gsub(/^\.\//,'')
    @author = nil
    case l
    when /\(C\)\s*(.*)$/i     # (C) ...
      @author = $1.strip
      @author = cleanup_author_year(@author)
      update_hash(@copyright, @fname, @author)
    when /Copyright\snotice[\r\n\)]+?(.*)/i
      @author = $1.strip
      @author = cleanup_author_year(@author)
      update_hash(@copyright, @fname, @author)
    when /Copyright[\s]+?(.*)/i
      @author = $1.strip
      @author = cleanup_author_year(@author)
      update_hash(@copyright, @fname, @author)
    when /\d{4},?\s+(.*)/     # 2004(,) ....
      @author = $1.gsub!(/by\s+/i, "")   # 2004 by ...
      @author = cleanup_author_year(@author)
      update_hash(@copyright, @fname, @author)
    when /by\s+(.*)$/i        # by ...
      @author = $1.strip
      @author = cleanup_author_year(@author)
      update_hash(@copyright, @fname, @author)
    else
      update_hash(@unknown, @fname, "unknown")
    end
  end
  @copyright.each do |k,v|
    @authors.push v
  end
  return @copyright, @authors.uniq.sort, @unknwon
end

copyright, authors, unknown = file_grep

authors.each do |author|
  data = copyright.find_all {|k,v| v == author}
  filename_line = "Files: "
  data.each do |fname|
    filename_line += fname[0] + " "
  end
  puts filename_line.rstrip
  author = author.split(",")
  puts "Copyright: (C) " + author.shift
  unless author.nil?
    author.each do |name|
      puts "           (C) " + name.strip
    end
  end
  puts "License: GPL-2.0+"
  puts ""
  copyright.reject!{|k,v| v == author}
end

puts unknown
