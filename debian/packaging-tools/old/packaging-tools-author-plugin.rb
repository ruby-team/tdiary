#!/usr/bin/env ruby

#
# Extract copyright holders.
# Author:   Daigo Moriwaki <beatles@sgtpepper.net>
# Copyright (c) 2005 Daigo Moriwaki
# License:  GNU GENERAL PUBLIC LICENSE Version 2 or later.
#
$KCODE = "E"

require 'pathname'

class Plugin < Struct.new(:name, :author, :copyright, :access, :license)
  def has_license?
    license && license.length > 0
  end

  def to_s
    s = ""
    s << "#{name}\n"
    s << copyright
    s << "\n\n"
    s
  end

  def ==(a)
    self.to_s == a.to_s
  end
end


def parse_readme(f)
  plugin = Plugin.new
  /(.*)\.rb/ =~ f.basename
  plugin.name = $1
  plugin.copyright = ""

  flag = nil
  f.each_line do |line|
    line.strip!
    if /copyright/i =~ line
      if /^#/ =~ line
        flag = :sharp
      else
        flag = :block
      end
    end

    if flag == :sharp
      if /^#(.*)$/ =~ line
        plugin.copyright += "  " + $1.strip + "\n"
      else
        break
      end
    elsif flag == :block
      next if /^=.*copyright/i =~ line 
      if /^=/ =~ line && 
        break
      else
        plugin.copyright += "  " + line + "\n"
      end
    end
  end
  
  plugin  
end


def main
  plugins = []
  Pathname.new('../misc/plugin').find do |f|
    plugins << parse_readme(f) if /\.rb$/ =~ f
  end
  plugins.sort! {|a,b| a.name <=> b.name}
  plugins.uniq!
  plugins.each {|p| print p.to_s}
end


main if __FILE__ == $0
