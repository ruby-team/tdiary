<!doctype refentry PUBLIC "-//OASIS//DTD DocBook V4.1//EN" [

<!-- Process this file with docbook-to-man to generate an nroff manual
     page: `docbook-to-man manpage.sgml > manpage.1'.  You may view
     the manual page with: `docbook-to-man manpage.sgml | nroff -man |
     less'.  A typical entry in a Makefile or Makefile.am is:

manpage.1: manpage.sgml
	docbook-to-man $< > $@

    
	The docbook-to-man binary is found in the docbook-to-man package.
	Please remember that if you create the nroff version in one of the
	debian/rules file targets (such as build), you will need to include
	docbook-to-man in your Build-Depends control field.

  -->

  <!-- Fill in your name for FIRSTNAME and SURNAME. -->
  <!ENTITY dhfirstname "<firstname>Moriwaki</firstname>">
  <!ENTITY dhsurname   "<surname></surname>">
  <!-- Please adjust the date whenever revising the manpage. -->
  <!ENTITY dhdate      "<date>April 17, 2005</date>">
  <!-- SECTION should be 1-8, maybe w/ subsection other parameters are
       allowed: see man(7), man(1). -->
  <!ENTITY dhsection   "<manvolnum>1</manvolnum>">
  <!ENTITY dhemail     "<email>beatles@sgtpepper.net</email>">
  <!ENTITY dhusername  "Daigo Moriwaki">
  <!ENTITY dhucpackage "<refentrytitle>TDIARY-SETUP</refentrytitle>">
  <!ENTITY dhpackage   "tdiary-setup">

  <!ENTITY debian      "<productname>Debian</productname>">
  <!ENTITY gnu         "<acronym>GNU</acronym>">
  <!ENTITY gpl         "<acronym>GPL Version 2</acronym>">
]>

<refentry>
  <refentryinfo>
    <address>
      &dhemail;
    </address>
    <author>
      &dhfirstname;
      &dhsurname;
    </author>
    <copyright>
      <year>2003</year>
      <holder>&dhusername;</holder>
    </copyright>
    &dhdate;
  </refentryinfo>
  <refmeta>
    &dhucpackage;

    &dhsection;
  </refmeta>
  <refnamediv>
    <refname>&dhpackage;</refname>
    <refpurpose>installer to set up tdiary files for a user</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <cmdsynopsis>
      <command>&dhpackage;</command>
      <group>
        <arg>default</arg>
        <arg>symlink</arg>
        <arg>copy</arg>
        <arg>update</arg>
      </group>
      <arg choice="plain"><replaceable>directory</replaceable></arg>
    </cmdsynopsis>
  </refsynopsisdiv>
  <refsect1>
    <title>DESCRIPTION</title>

    <para>This manual page documents briefly the
      <command>&dhpackage;</command> commands.</para>

    <!-- 
    <para>This manual page was written for the &debian; distribution
      because the original program does not have a manual page.
      Instead, it has documentation in the &gnu;
      <application>Info</application> format; see below.</para>

    <para><command>&dhpackage;</command> is a installer program 
      that sets up tdiary files to user's directory.</para>
    -->

  </refsect1>
  <refsect1>
    <title>OPTIONS</title>

    <para>These programs follow the usual GNU command line syntax,
      with long options starting with two dashes (`-').  A summary of
      options is included below.  For a complete description, see the
      <application>Info</application> files.</para>

    <variablelist>
      <varlistentry>
        <term>default</term>
        <listitem>
          <para>Copy CGI files and set up a tdiary user. 
            Choose default if your httpd runs under suEXEC mode, 
            which is generally recommended to make security stronger.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>symlink</term>
        <listitem>
          <para>Make symbolic links and set up a tdiary user. 
           Choose symlink if your httpd does not run under suEXEC mode.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>copy</term>
        <listitem>
          <para>Copy all the files and set up a tdiary user. </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>update</term>
        <listitem>
          <para>Update existing tDiary setting. 
          Use this when you update the tdiary package.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><replaceable>directory</replaceable></term>
        <listitem>
          <para>Directory where CGI files will be copied. If the directory does
            not exist it will be made.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsect1>
  <refsect1>
    <title>USAGE</title>
    <para>
      <command>&dhpackage;</command> default /home/foo/public_html/diary
    </para>
  </refsect1>  
  <refsect1>
    <title>AUTHOR</title>

    <para>This manual page was written by &dhusername; &dhemail; for
      the &debian; system (but may be used by others).  Permission is
      granted to copy, distribute and/or modify this document under
		the terms of the &gpl;.</para> 
  </refsect1>
</refentry>

<!-- Keep this comment at the end of the file
Local variables:
mode: sgml
sgml-omittag:t
sgml-shorttag:t
sgml-minimize-attributes:nil
sgml-always-quote-attributes:t
sgml-indent-step:2
sgml-indent-data:t
sgml-parent-document:nil
sgml-default-dtd-file:nil
sgml-exposed-tags:nil
sgml-local-catalogs:nil
sgml-local-ecat-files:nil
End:
-->


