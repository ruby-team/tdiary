# Changes theme URL for debian.
#
# Author: Daigo Moriwaki <beatles@sgtpepper.net>
# Copyright (c) 2004 Daigo Moriwaki
# Copyright (c) 2013-2015 Youhei SASAKI
# License: GPL version 2 or later

def theme_url
  'theme'
end

def js_url
  'js'
end
