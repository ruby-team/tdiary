#!/usr/bin/env ruby

# Unit tests for tdiary-setup.rb
# Author: Daigo Moriwaki <beatles@sgtpepper.net>
# Copyright (c) 2004 Daigo Moriwaki
# License: GPL version 2 or later 
 
require 'test/unit'
require 'tdiary-setup'

class InputTest < Test::Unit::TestCase
	def setup
		@params = TDiarySetup::Parameters.new
		@input = TDiarySetup::Input.new(@params)
		class << @input
         def print(s)
				# do nothing
			end
		end
	end

	def test_read_with_default
		class << @input
			def read; "\n" end
		end
		default = "something as default"
		res = @input.read_with_default(default)
		assert_equal(default, res)
	end

	def test_ask_language
		class << @input
			def read
				"j\n"
			end
			def confirm; "Y\n" end
		end
		@input.ask_language
		assert_equal("j", @params.language)
	end

	def test_ask_language_chinese
		class << @input
			def read
				"z\n"
			end
			def confirm; "Y\n" end
		end
		@input.ask_language
		assert_equal("z", @params.language)
	end
  
	def test_ask_language_default
		class << @input
			def read
				"\n"
			end
			def confirm; "Y\n" end
		end
		@input.ask_language
		assert_equal("E", @params.language)
	end

	def test_ask_data_path
		class << @input
			def read
				"/home/hoge/data\n"
			end
		end
		@input.ask_data_path
		assert_equal("/home/hoge/data", @params.data_path)
	end

	def test_ask_data_path_default
		class << @input
			def read
				"\n"
			end
			def confirm; "Y\n" end
		end
		@input.ask_data_path
		assert_equal("/home/daigo/data", @params.data_path)
	end
end # InputTest


class ParametersTest < Test::Unit::TestCase
   def setup
      @params = TDiarySetup::Parameters.new
	end

	def test_language_type_english
      @params.language = "e"
		assert_equal(:en, @params.language_type)
	end
	
	def test_language_type_japanese
      @params.language = "j"
		assert_equal(:ja, @params.language_type)
	end
	
	def test_image_dir
      @params.target = "/hoge"
		assert_equal("/hoge/images", @params.image_dir)
	end
end # ParametersTest


class InstallhtpasswdTest < Test::Unit::TestCase
	def setup
		@params  = TDiarySetup::Parameters.new
		webrick = WEBrick::HTTPAuth::Htpasswd.new("/home/daigo/.htpasswd")
		class << webrick
			def reload
				@passwd["hoge"]  = "eB2qa20Du5fts"
				@passwd["daigo"] = "iykFnDSAUP0ic"
				@passwd["foo"]   = "5dnWnHmyn955g"
			end

			def flush
				# do nothing
			end
		end
		webrick.reload
		@ht = TDiarySetup::Installhtpasswd.new(@params, webrick)
		class << @ht
         def print(s)
				# do nothing
			end
		end
	end

	def test_user_exists?
		assert(@ht.user_exists?("hoge"))
	end

	def test_user_exists_not?
		assert(! @ht.user_exists?("nobody"))
	end

	def test_user_create
		@ht.create_entry("11111", "91111")
		assert(@ht.user_exists?("11111"))
	end

	def test_ask
		@params.user = "222"
		class << @ht
         def read
			   return "aaa\n".strip
			end
		end
		pass = @ht.ask
		assert_equal("aaa", pass)
		assert(@ht.user_exists?("222"))
	end
end # InstallhtpasswdTest


class DefaultSetupTest < Test::Unit::TestCase 
	def setup
		params = TDiarySetup::Parameters.new
		params.target = "."
      @setup = TDiarySetup::DefaultSetup.new(params)
		class << @setup
         include Test::Unit::Assertions
			
         def expected_mkdir(count)
            @expected_mkdir_count = count
			end
			
			def mkdir(mode, dir)
            @mkdir_count ||= 0
				@mkdir_count += 1
			end

			def expected_makerss(count)
            @makerss_count = 0
            @expected_makerss = count
			end
			
			def install_makerss
				@makerss_count += 1
			end
			
			def verify
            assert_equal(@expected_mkdir_count, @mkdir_count)
				assert_equal(@expected_makerss,     @makerss_count)
			end

			def puts(s)
				# do nothing
			end
		end
		
		class << FileUtils
			include Test::Unit::Assertions
			@@count = 0

         def FileUtils.expected_count(count)
            @@expected_count = count
			end
			
			def FileUtils.install(*args)
				@@count += 1
			end

			def FileUtils.verify
            assert_equal(@@expected_count, @@count)
			end
		end
	end

	def test_setup_with_plugin_package
		class << @setup
			def hasPluginPackage? 
				true 
			end
		end
		@setup.expected_mkdir 3
		FileUtils.expected_count 4
      @setup.expected_makerss 1
		
		@setup.install
		FileUtils.verify
		@setup.verify
	end

	def test_setup_without_plugin_package
		class << @setup
			def hasPluginPackage? 
				false 
			end
		end
		@setup.expected_mkdir 3
		FileUtils.expected_count 2
      @setup.expected_makerss 0
		
		@setup.install
		FileUtils.verify
		@setup.verify
	end
end # DefaultSetupTest


class SymlinkSetupTest < Test::Unit::TestCase 
	def setup
		params = TDiarySetup::Parameters.new
		params.target = "."
      @setup = TDiarySetup::SymlinkSetup.new(params)
		class << @setup
         include Test::Unit::Assertions
			
         def expected_mkdir(count)
            @expected_mkdir_count = count
			end

			def mkdir(mode, dir)
            @mkdir_count ||= 0
				@mkdir_count += 1
			end

			def expected_makerss(count)
            @makerss_count = 0
            @expected_makerss = count
			end
			
			def install_makerss
				@makerss_count += 1
			end

			def verify
            assert_equal(@expected_mkdir_count, @mkdir_count)
				assert_equal(@expected_makerss,     @makerss_count)
			end
			
			def puts(s)
				# do nothing
			end
		end
		
		class << FileUtils
			include Test::Unit::Assertions
			@@count = 0

         def FileUtils.expected_count(count)
            @@expected_count = count
			end
			
			def FileUtils.ln_sf(*args)
				@@count += 1
			end

			def FileUtils.verify
            assert_equal(@@expected_count, @@count)
			end
		end
	end

	def test_setup_with_plugin_package
		class << @setup
			def hasPluginPackage? 
				true 
			end
		end
		@setup.expected_mkdir 3
		FileUtils.expected_count 4
		@setup.expected_makerss 1
		
		@setup.install
		FileUtils.verify
		@setup.verify
	end

	def test_setup_without_plugin_package
		class << @setup
			def hasPluginPackage? 
				false 
			end
		end
		@setup.expected_mkdir 3
		FileUtils.expected_count 2
		@setup.expected_makerss 0
		
		@setup.install
		FileUtils.verify
		@setup.verify
	end
end # SymlinkSetupTest


class CopySetupTest < Test::Unit::TestCase 
	def setup
		params = TDiarySetup::Parameters.new
		params.target = "."
      @setup = TDiarySetup::CopySetup.new(params)
		class << @setup
         include Test::Unit::Assertions
			
         def expected_mkdir(count)
            @expected_mkdir_count = count
			end
			
			def mkdir(mode, dir)
            @mkdir_count ||= 0
				@mkdir_count += 1
			end

			def verify
            assert_equal(@expected_mkdir_count, @mkdir_count)
			end
			
			def puts(s)
				# do nothing
			end
		end
		
		class << FileUtils
			include Test::Unit::Assertions
			@@count = 0

         def FileUtils.expected_count(count)
            @@expected_count = count
			end
			
			def FileUtils.cp_r(*args)
				@@count += 1
			end

			def FileUtils.verify
            assert_equal(@@expected_count, @@count)
			end
		end
	end

	def test_install
		@setup.expected_mkdir 3
		FileUtils.expected_count 1
		@setup.install
		FileUtils.verify
		@setup.verify
	end
end # CopySetupTest


class InstallhtaccessTest < Test::Unit::TestCase
	def setup
      @params = TDiarySetup::Parameters.new("test1", "/home/test1")
		@htaccess = TDiarySetup::Installhtaccess.new(@params)
	end
	
	def test_sub
      lines = <<-FILE
#Options +FollowSymLinks
AuthUserFile  /home/foo/.htpasswd
		FILE
		result = @htaccess.sub(lines)
		assert_match(/^Options \+FollowSymLinks/, result)
		assert_match(%r!AuthUserFile  /home/test1/\.htpasswd!, result)
	end
end # InstallhtaccessTest


class InstalltdiaryconfTest < Test::Unit::TestCase
   def setup
      @params = TDiarySetup::Parameters.new
		@tdconf = TDiarySetup::Installtdiaryconf.new(@params)
		@lines = <<-LINES
#   $B$7$F$*$/I,MW$,$"$j$^$9!#(B
@data_path = '/home/foo/diary'
#   $B%j$r;XDj$9$k$3$H$b2DG=$G$9!#(B
#@cache_path = 'path of cache'
load_cgi_conf
		LINES
	end

	def test_data_path
		@params.data_path = "/home/testdatapath/data"
      result = @tdconf.sub(@lines)
		assert_match(%r!^@data_path = '/home/testdatapath/data'$!, result)
	end

	def test_cache_path
      @params.user = "testcache"
		result = @tdconf.sub(@lines)
		assert_match(%r!^@cache_path = '/tmp/testcache_\d+'!, result)
	end

	def test_load_cgi_conf
      result = @tdconf.sub(@lines)
		assert_match(%r!^@options\['sp\.path'\]!, result)
	end
end  # InstalltdiaryconfTest


class CheckModeTest < Test::Unit::TestCase
   def setup
      @check = TDiarySetup::CheckMode.new(TDiarySetup::Parameters.new())
	end

	def test_symlink
      class << @check
         def symlink?
            true
			end

			def copy?
            false
			end
		end
		assert_equal(:symlink, @check.mode)
	end

	def test_copy
      class << @check
         def symlink?
            false
			end

			def copy?
            true
			end
		end
		assert_equal(:copy, @check.mode)
	end

	def test_default
      class << @check
         def symlink?
            false
			end

			def copy?
            false
			end
		end
		assert_equal(:default, @check.mode)
	end
end # CheckModeTest


class WEBrickUtilsTest < Test::Unit::TestCase
   def test_random_string
      str = WEBrick::Utils.random_string(2)
		assert_match(/^[A-Za-z0-9][A-Za-z0-9]$/, str)
	end
end

module WEBrick
   module Utils
	   def random_string(len)
         "aa"
		end
		module_function :random_string
	end
end

class BasicAuthTest < Test::Unit::TestCase
   def test_make_passwd
      str = WEBrick::HTTPAuth::BasicAuth::make_passwd("","", "hoge")
		assert_equal("hoge".crypt("aa"), str)
	end
end

